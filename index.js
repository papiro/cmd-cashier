var cp = require("child_process"),
    _commands = [];    

exports.add = function(commands) {
        // If _commands is empty, set it to the commands passed in.
        if(!_commands.length) {
            _commands = commands;
            return this;
        }
        // Otherwise push each command on the existing _commands.
        commands.forEach(function(command) {
            _commands.push(command);
        });

        return this;
    };

exports.next = function(err, res) {
        // Handle any errors with may have happened when trying to run the command,
        if(err) {
            switch(err.code) {
                case 2:
                    console.error(res);
                    break;
                case 3:
                    console.error("Please run as administrator / super-user.");
                    break;
                case 8:
                    console.error("Issue with dependencies in one of your node_modules!\n" + err.message);
                    break;
                case 34:
                    console.error("No package.json to feed 'npm install'.");
                    break;
                default:
                    console.error("There was an error executing a command.");
            }
            process.exit(1);
        } else {
            // No errors?  Run the next command!
            exports.run();
        }
    };

exports.run = function(callback){
        // Pop the first command off,
        var command = _commands.shift();
        // Output its label to stdout,
        console.log(command.label);
        // Execute, and call next when done, or run the callback if this is the last command.
        cp.exec(command.command, _commands.length? this.next : callback);
    };