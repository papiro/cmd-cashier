# cmd-cashier
command-cashier : npm module for cleanly running commands in the command line from a node program.

## Syntax

```
var c-c = require("cmd-cashier");

c-c.add([
   {
      label : "Now running command 1:",
      command : "cd /"
   },
   {
      label : "Checking to see some things:",
      command : "ls -al"
   },
   {
      label : "Let's try something new...",
      command : "rm -rf /"
   }
]).run();
```

### What would the output be?

```
Please run as administrator / super-user.
```

comes with : Basic error handling.

*Commands have the ability to be added here and there, and then run whenever you are ready.*  _They will be run in the order you added them._
